﻿using System.Web.Routing;

namespace MistaSmartTools.Web
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.Ignore("{resource}.axd/{*pathInfo}"); //ignore httphandlers 

            //resolve the file favicon.ico does not exist exception   
            routes.Ignore("favicon.ico");

            // reroute for all the angular applications/pages

            // main application
            routes.MapPageRoute("main", "", "~/pages/index.html");


            // any routes created by server-side should be configured in agularjs app as htmlmode ON. 

            // viewer application
            routes.MapPageRoute("event-viewer", "{id}", "~/pages/indexviewer.html");

            //routes.MapRoute(
            //    name: "Default",
            //    url: "{controller}/{action}/{id}",
            //    defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            //);



        }
    }
}