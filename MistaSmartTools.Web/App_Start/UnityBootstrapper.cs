﻿using Microsoft.Practices.Unity;
using MistaSmartTools.Core.Interfaces;
using MistaSmartTools.Infrastructure;
using MistaSmartTools.Web.Interfaces;
using MistaSmartTools.Web.Controllers;
using System.Web.Http;

namespace MistaSmartTools.Web.App_Start
{
    public static class UnityBootstrapper
    {
        public static IUnityContainer Container
        {
            get; set;
        }

        public static void Bootstrap(HttpConfiguration config)
        {
            Container = new UnityContainer();
            RegisterTypes();
            config.DependencyResolver = new UnityResolver(Container);
        }

        private static void RegisterTypes()
        {
            Container.RegisterType<IPokerRankingTable, PokerHandRankingTable>(new HierarchicalLifetimeManager());
            var pokerRankingTable = Container.Resolve<IPokerRankingTable>();
            Container.RegisterInstance(pokerRankingTable);

            Container.RegisterType<IHand, Hand>(new HierarchicalLifetimeManager());
            var hand = Container.Resolve<IHand>();
            //Container.RegisterInstance(hand);
        }
    }
}