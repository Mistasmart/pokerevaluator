﻿using MistaSmartTools.Core.Enums;
using MistaSmartTools.Core.Interfaces;

// NOTE: The Hand and Deck classes are not yet unit testable. Ideally, we would never reference the Infrastructure project at this level.
//  To refactor this, we would have create a factory class that is Unit testable w/ it's relative interface. This factory class would contain methods
//  to create instances of a Deck or Hand.
using MistaSmartTools.Infrastructure;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace MistaSmartTools.Web.Controllers
{
    public class PokerController : ApiController
    {
        private readonly IPokerRankingTable _pokerRankingTable;

        public PokerController(IPokerRankingTable pokerRankingTable)
        {
            _pokerRankingTable = pokerRankingTable;
        }

        [HttpGet]
        public Object GetSampleResult()
        {
            try
            {
                //var h1 = new HandModel
                //{
                //    PlayerName = "John Doe",
                //    Card1 = new CardModel { CardValue = CardEnum.Ace, CardSuit = SuitEnum.Clubs },
                //    Card2 = new CardModel { CardValue = CardEnum.Ace, CardSuit = SuitEnum.Hearts },
                //    Card3 = new CardModel { CardValue = CardEnum.Ace, CardSuit = SuitEnum.Spades },
                //    Card4 = new CardModel { CardValue = CardEnum.Ace, CardSuit = SuitEnum.Diamonds },
                //    Card5 = new CardModel { CardValue = CardEnum.Two, CardSuit = SuitEnum.Clubs },
                //};
                //var h2 = new HandModel
                //{
                //    PlayerName = "James Bond",
                //    Card1 = new CardModel { CardValue = CardEnum.King, CardSuit = SuitEnum.Clubs },
                //    Card2 = new CardModel { CardValue = CardEnum.King, CardSuit = SuitEnum.Hearts },
                //    Card3 = new CardModel { CardValue = CardEnum.King, CardSuit = SuitEnum.Spades },
                //    Card4 = new CardModel { CardValue = CardEnum.King, CardSuit = SuitEnum.Diamonds },
                //    Card5 = new CardModel { CardValue = CardEnum.Two, CardSuit = SuitEnum.Hearts },
                //};
                var deck = new Deck();
                var h1 = new Hand(deck.Pop(), deck.Pop(), deck.Pop(), deck.Pop(), deck.Pop(), _pokerRankingTable);
                var h2 = new Hand(deck.Pop(), deck.Pop(), deck.Pop(), deck.Pop(), deck.Pop(), _pokerRankingTable);

                h1.playerName = "Bob Johnson (Player1)";
                h2.playerName = "James Smith (Player2)";
                var hands = new List<IHand>();
                hands.Add(h1);
                hands.Add(h2);

                hands = hands.OrderBy(h => h.Rank).ToList<IHand>();
                var retVal = string.Format("{0} and is the winning hand. {1}.",
                    hands[0].ToString(HandToStringFormatEnum.PlayerHandDescription),
                    hands[1].ToString(HandToStringFormatEnum.PlayerHandDescription));
                if (hands[0] == hands[1])
                {
                    retVal = hands[0].ToString(HandToStringFormatEnum.HandDescription) +
                          " ties with " + hands[1].ToString(HandToStringFormatEnum.HandDescription);
                }

                return new
                {
                    HandResultDescription = retVal,
                    Hand1 = new
                    {
                        Rank = h1.Rank,
                        PlayerName = h1.playerName,
                        Cards = new object[] {
                            new { suit = h1.mC1.CardSuit.ToString(), letter = h1.mC1.CardValue.ToString() },
                            new { suit = h1.mC2.CardSuit.ToString(), letter = h1.mC2.CardValue.ToString() },
                            new { suit = h1.mC3.CardSuit.ToString(), letter = h1.mC3.CardValue.ToString() },
                            new { suit = h1.mC4.CardSuit.ToString(), letter = h1.mC4.CardValue.ToString() },
                            new { suit = h1.mC5.CardSuit.ToString(), letter = h1.mC5.CardValue.ToString() },
                        }
                    },
                    Hand2 = new
                    {
                        Rank = h2.Rank,
                        PlayerName = h2.playerName,
                        Cards = new object[] {
                            new { suit = h2.mC1.CardSuit.ToString(), letter = h2.mC1.CardValue.ToString() },
                            new { suit = h2.mC2.CardSuit.ToString(), letter = h2.mC2.CardValue.ToString() },
                            new { suit = h2.mC3.CardSuit.ToString(), letter = h2.mC3.CardValue .ToString()},
                            new { suit = h2.mC4.CardSuit.ToString(), letter = h2.mC4.CardValue.ToString() },
                            new { suit = h2.mC5.CardSuit.ToString(), letter = h2.mC5.CardValue.ToString() },
                        }
                    }
                };
            }
            catch
            {
                // If we got this far, something failed
                throw new Exception("Unable to evaluate poker hands.");
            }
        }
    }
}