﻿appRoot.factory('httpRequestTracker', ['$http', function ($http)
{
    var httpRequestTracker = {};
    httpRequestTracker.hasPendingRequests = function () {
        if ($http.pendingRequests.length === 0){
            return false;
        }
        
        for (var i = 0; i < $http.pendingRequests.length; i++)
        {
            var bIsBackgroundRequest = false;

            //// doesn't start with ...
            //if ($http.pendingRequests[0].url.indexOf('api/Sites/GetMostRecentPublishSiteResults') > -1)
            //{
            //    bIsBackgroundRequest = true;
            //}

            // if a request is not a background request, return true.
            if (!bIsBackgroundRequest)
                return true;
        }

        return false;
    };

    return httpRequestTracker;
}]);

appRoot.factory('stringHelper', [function () {
    var stringHelper = {};

    stringHelper.IsNullOrWhiteSpace = function (str) {
        return !str || str.match(/^ *$/) !== null;
    };

    return stringHelper;
}]);