﻿appRoot.controller('IndexCtrl', ['$scope', '$http', '$route', '$routeParams', '$location', '$q', 'httpRequestTracker', '$timeout',
function ($scope, $http, $route, $routeParams, $location, $q, httpRequestTracker, $timeout)
{    
    $scope.user =
    {
        username: '',
        isAuthenticated: false
    };

    $scope.messageHandler = undefined;
    
    $scope.settings = undefined;
    
    $scope.getQueryStringValue = function (key)
    {
        return unescape(window.location.href.replace(new RegExp("^(?:.*[&\\?]" + escape(key).replace(/[\.\+\*]/g, "\\$&") + "(?:\\=([^&]*))?)?.*$", "i"), "$1"));
    };
    
    $scope.hasPendingRequests = function ()
    {
        return httpRequestTracker.hasPendingRequests();
    };

    $scope.dateFormat = 'MMM dd, yyyy h:mm a';
    
    $scope.init = function ()
    {   
        $scope.messageHandler = { };
        
    };
    
    $scope.hideMainMessage = function ()
    {
        $scope.messageHandler.hide();
    };

    $scope.showMainMessage = function (message, autohideTimePeriod) {
        $scope.messageHandler.showMessage(message, autohideTimePeriod);
    };
    
    $scope.showMainError = function (error)
    {
        $scope.messageHandler.showError(error);
    };
    
    $scope.$on('$routeChangeSuccess', function (e, current, previous)
    {
        $scope.activeViewPath = $location.path();
    });
}]);