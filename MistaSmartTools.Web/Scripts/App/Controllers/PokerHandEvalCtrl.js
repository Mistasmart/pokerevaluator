﻿
appRoot.controller('PokerHandEvalCtrl', ['$scope', '$http', '$location', '$q', '$timeout', '$modal', '$cookieStore', '$routeParams', '$sce', function ($scope, $http, $location, $q, $timeout, $modal, $cookieStore, $routeParams, $sce) {

    $scope.handsResult = undefined;

    $scope.messageHandler = {};

    $scope.init = function () {
        $scope.loadSampleResult().then(function () {
           
        });
    };

    $scope.loadSampleResult = function () {

        var deferred = $q.defer();
        $http.get('api/Poker/GetSampleResult')
            .success(function (data) {
                $scope.handsResult = data;
                $scope.handsResult.Hand1.Cards = [$scope.Card(data.Hand1.Cards[0]),
                    $scope.Card(data.Hand1.Cards[1]),
                    $scope.Card(data.Hand1.Cards[2]),
                    $scope.Card(data.Hand1.Cards[3]),
                    $scope.Card(data.Hand1.Cards[4])];
                $scope.handsResult.Hand2.Cards = [$scope.Card(data.Hand2.Cards[0]),
                    $scope.Card(data.Hand2.Cards[1]),
                    $scope.Card(data.Hand2.Cards[2]),
                    $scope.Card(data.Hand2.Cards[3]),
                    $scope.Card(data.Hand2.Cards[4])];

                $scope.messageHandler.showMessage($scope.handsResult.HandResultDescription, 9999999);

                deferred.resolve();
            })
            .error(function (error) {
                $scope.messageHandler.showError(error);
                deferred.resolve();
            });

        return deferred.promise;
    };

    $scope.suits = [{
        name: 'Clubs',
        color: 'black',
        glyph: $sce.trustAsHtml('&#x2663;')
    }, {
        name: 'Spades',
        color: 'black',
        glyph: $sce.trustAsHtml('&#x2660;')
    }, {
        name: 'Hearts',
        color: 'red',
        glyph: $sce.trustAsHtml('&#x2665;')
    }, {
        name: 'Diamonds',
        color: 'red',
        glyph: $sce.trustAsHtml('&#x2666;')
    }];

    // the card object
    $scope.Card = function Card(remoteCard) {
        let suit = null;
        for (let i = 0; i < $scope.suits.length; i++) {
            if (remoteCard.suit.toLowerCase() == $scope.suits[i].name.toLowerCase()) {
                suit = $scope.suits[i];
                break;
            }
        }
        let letter = remoteCard.letter;
        if (letter.toLowerCase() == "two") {
            letter = "2"
        } else if (letter.toLowerCase() == "three") {
            letter = "3"
        } if (letter.toLowerCase() == "four") {
            letter = "4"
        } if (letter.toLowerCase() == "five") {
            letter = "5"
        } if (letter.toLowerCase() == "six") {
            letter = "6"
        } if (letter.toLowerCase() == "seven") {
            letter = "7"
        } if (letter.toLowerCase() == "eight") {
            letter = "8"
        } if (letter.toLowerCase() == "nine") {
            letter = "9"
        } if (letter.toLowerCase() == "ten") {
            letter = "10"
        }

        return {
            suit: suit,
            letter: letter,
            name: name || letter,
            displayName: (name || letter) + ' of ' + suit.name
        };
    }

}]);