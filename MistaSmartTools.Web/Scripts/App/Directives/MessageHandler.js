﻿appRoot.directive('messagehandler', function ($timeout) {
    return {
        restrict: 'E',
        transclude: true,
        scope: {
            messageobj: '='
        },
        templateUrl: 'Templates/MessageHandler.html?v=##VERSION##',
        replace: true,
        link: link
    };
    
    function link($scope, element, attrs)
    {
        $scope.messageobj.showMessage = function (message, autohideTimePeriod)
        {
            $scope.messageobj.$isVisible = true;
            $scope.messageobj.message = message;
            $scope.messageobj.isError = false;

            if (autohideTimePeriod) $scope.messageobj.autohideTimePeriod = autohideTimePeriod;
            
            bind($scope);
        };
        
        $scope.messageobj.showError = function (error)
        {
            $scope.messageobj.$isVisible = true;
            $scope.messageobj.message = error;
            $scope.messageobj.isError = true;
            bind($scope);
        };
        
        $scope.messageobj.hide = function ()
        {
            $scope.messageobj.$isVisible = false;
        };
    }
    
    function bind($scope)
    {
        if (!$scope.messageobj)
        {
            return;
        }

        if ($scope.messageobj.isError)
        {
            var error = $scope.messageobj.message;

            if (error.ExceptionMessage)
            {
                $scope.messageobj.message = error.ExceptionMessage;
            }
            else if (error.MessageDetail)
            {
                $scope.messageobj.message = error.MessageDetail;
            }
            else if (error.Message)
            {
                $scope.messageobj.message = error.Message;
            }
            else if (error.ErrorMessage)
            {
                $scope.messageobj.message = error.ErrorMessage;
            }
            else if (typeof $scope.messageobj.message !== 'string')
            {
                $scope.messageobj.message = "Server error.";
            }

            return;
        }

        if ($scope.messageobj.$isVisible)
        {
            $timeout(function ()
            {
                $scope.messageobj.hide();
                $scope.messageobj.autohideTimePeriod = 3000;
            }, $scope.messageobj.autohideTimePeriod ? $scope.messageobj.autohideTimePeriod : 3000); // default to 3 secs if not explicitly indicated.
        }
    }
});