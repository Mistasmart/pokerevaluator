﻿appRoot.directive('enter', function()
{
    return function(scope, element, attrs)
    {
        element.bind("keydown", function(event)
        {
            if (event.which == 13)
            {
                scope.$apply(function()
                {
                    scope.$eval(attrs.enter);
                });
            }
        });
    };
});