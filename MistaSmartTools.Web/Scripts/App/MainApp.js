﻿// Main configuration file. Sets up AngularJS module and routes and any other config objects

var appRoot = angular.module('main', ['ngRoute', 'mgcrea.ngStrap', 'textAngular', 'infinite-scroll', 'ngCookies', 'xeditable']);     //Define the main module

appRoot
    .config(['$routeProvider', '$locationProvider', '$httpProvider', function ($routeProvider, $locationProvider, $httpProvider)
    {
        //Setup routes to load partial templates from server. TemplateUrl is the location for the server view (Razor .cshtml view)
        $routeProvider
            .when('/poker', { templateUrl: 'views/poker/pokerhandeval.html?v=##VERSION##', controller: 'PokerHandEvalCtrl' })
            .otherwise({ redirectTo: '/poker' });


            // current angular version: http doesn't handle null values correctly.
            $httpProvider.defaults.transformResponse.push(function (data) {
                if (data === "null") {
                    data = null;
                }
                return data;
            });
       //$locationProvider.html5Mode(true);
    }]);

appRoot
    .run(function (editableOptions)
    {
        editableOptions.theme = 'bs3'; // bootstrap3 theme. Can be also 'bs2', 'default'
    });
