﻿using MistaSmartTools.Core.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MistaSmartTools.Core.Interfaces
{
    public interface IHand
    {
        int Rank { get; }
        string ToString(HandToStringFormatEnum Format);
    }
}
