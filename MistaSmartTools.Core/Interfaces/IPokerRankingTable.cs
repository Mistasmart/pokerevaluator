﻿using MistaSmartTools.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MistaSmartTools.Core.Interfaces
{
    public interface IPokerRankingTable
    {
        SortedList<int, EvalHand> EvalHands { get; set; }
    }
}
