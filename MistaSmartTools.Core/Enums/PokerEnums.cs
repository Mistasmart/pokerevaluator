﻿namespace MistaSmartTools.Core.Enums
{
    /// <summary>
    /// Enumeration values are used to calculate hand rank keys
    /// </summary>
    public enum CardEnum : int
    {
        Two = 2,
        Three = 3,
        Four = 5,
        Five = 7,
        Six = 11,
        Seven = 13,
        Eight = 17,
        Nine = 19,
        Ten = 23,
        J = 29,
        Q = 31,
        K = 37,
        A = 41
    }
    public enum SuitEnum : int
    {
        Spades,
        Hearts,
        Clubs,
        Diamonds
    }
    public enum CardToStringFormatEnum
    {
        ShortCardName,
        LongCardName
    }

    public enum HandToStringFormatEnum
    {
        ShortCardsHeld,
        LongCardsHeld,
        HandDescription,
        PlayerHandDescription
    }

}
