﻿using MistaSmartTools.Core.Enums;
namespace MistaSmartTools.Core.Models
{
    public class CardModel
    {
        public CardModel()
        {
        }

        public CardEnum CardValue { get; set; }
        public SuitEnum CardSuit { get; set; }

        public override string ToString()
        {
            return ToString(CardToStringFormatEnum.LongCardName);
        }

        public string ToString(CardToStringFormatEnum format)
        {
            switch (format)
            {
                case CardToStringFormatEnum.LongCardName:
                    {
                        return CardValue.ToString() + " of " + CardSuit.ToString();
                    }

                case CardToStringFormatEnum.ShortCardName:
                    {
                        switch (CardValue)
                        {
                            case CardEnum.Two:
                                {
                                    return "2" + CardSuit.ToString().Substring(0, 1).ToLower();
                                }

                            case CardEnum.Three:
                                {
                                    return "3" + CardSuit.ToString().Substring(0, 1).ToLower();
                                }

                            case CardEnum.Four:
                                {
                                    return "4" + CardSuit.ToString().Substring(0, 1).ToLower();
                                }

                            case CardEnum.Five:
                                {
                                    return "5" + CardSuit.ToString().Substring(0, 1).ToLower();
                                }

                            case CardEnum.Six:
                                {
                                    return "6" + CardSuit.ToString().Substring(0, 1).ToLower();
                                }

                            case CardEnum.Seven:
                                {
                                    return "7" + CardSuit.ToString().Substring(0, 1).ToLower();
                                }

                            case CardEnum.Eight:
                                {
                                    return "8" + CardSuit.ToString().Substring(0, 1).ToLower();
                                }

                            case CardEnum.Nine:
                                {
                                    return "9" + CardSuit.ToString().Substring(0, 1).ToLower();
                                }

                            case CardEnum.Ten:
                                {
                                    return "T" + CardSuit.ToString().Substring(0, 1).ToLower();
                                }

                            case CardEnum.J:
                                {
                                    return "J" + CardSuit.ToString().Substring(0, 1).ToLower();
                                }

                            case CardEnum.Q:
                                {
                                    return "Q" + CardSuit.ToString().Substring(0, 1).ToLower();
                                }

                            case CardEnum.K:
                                {
                                    return "K" + CardSuit.ToString().Substring(0, 1).ToLower();
                                }

                            case CardEnum.A:
                                {
                                    return "A" + CardSuit.ToString().Substring(0, 1).ToLower();
                                }
                        }

                        break;
                    }
            }

            return "<Card value not set>";
        }
    }

    public class EvalHand
    {
        public int Key { get; }
        public int Rank { get; }
        public string Name { get; }

        public EvalHand(int key, int rank, string name)
        {
            Key = key;
            Rank = rank;
            Name = name;
        }
    }
}
