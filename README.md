# PokerEvaluator

Reference/Credits
------

**C# Poker Hand Evaluator**
https://github.com/jessechunn/StandardPokerHandEvaluator
With this repository, I was able to save much time. I mainly use it to evaluate a winning poker hand. I tweaked some of the code to make some classes unit testable. I also did some minor tweaks in order to customize towards the problem at hand.

**AngularJS Deck of cards**
https://embed.plnkr.co/1LsJTl/
With this source,  I am only utilizing the [suits] and [card] objects. It helped me significantly on how I can dislay a card on a page/view.
----------------------------------------------------------------------------------------------------------------------------

How To Run:
------
* Open solution using Visual Studio.
* Set the project **[MistaSmartTools.Web]** as the startup project (right click on the project -> Set as StartUp Project)
* Run the project in Visual Studio using IIS Express.
* When the project runs, it should open a browser w/ the URL: localhost:portnumber/#/poker
	- if it doesn't, manually type in the correct URL.


----------------------------------------------------------------------------------------------------------------------------

Summary On My Solution
------
When the poker view (localhost:portnumber/#/poker) initially loads, it will display a simulated hand between 2 players. The display will show each player's name and their 5 holding cards. A green checkmark should appear next the winning player's name.

You also should see a button [Run another test] on this view. You can run as many randomized poker hands simulation as possible by clicking this button.


----------------------------------------------------------------------------------------------------------------------------

Artitecture
------
I followed the Onion Artitechture w/ some dependency injections to show that this solution can be unit testable.


----------------------------------------------------------------------------------------------------------------------------